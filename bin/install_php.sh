#!/bin/bash
# phpenv: https://github.com/madumlao/phpenv

env_name="phpenv"
script_dir=$(dirname $(realpath $0))
anyenv_cmd="${HOME}/.anyenv/bin/anyenv"

[ -z $(type -t get_var) ] && source "${script_dir}/../etc/env.sh"

## build dependencies:
# TODO: build error 5.6.40 -> NG, 7.2.25 / 7.3.13 -> OK
if [ $(get_var OS_TYPE) == "GNU/Linux" ] ; then
  $(get_var APT_GET) update && $(get_var APT_GET) install -y \
      g++ bison re2c pkg-config libssl-dev libbz2-dev \
      libxml2-dev libxslt1-dev libcurl4-openssl-dev libicu-dev \
      libmcrypt-dev zlib1g-dev libjpeg-dev libpng-dev \
      libtidy-dev libreadline-dev libzip-dev \
      libkrb5-dev libsqlite3-dev libonig-dev
elif [ $(get_var OS_TYPE) == "Android" ] ; then
  # TODO: termux required package
  echo "I have not examined it yet."
  # $(get_var APT_GET) update && $(get_var APT_GET) install -y
fi

if [ -z $(type -P ${env_name}) ]; then
  ${anyenv_cmd} install ${env_name}
  eval "$(${anyenv_cmd} init -)"
fi

# env_root=$(${env_name} root)

## plugins:

##
${env_name} install $(get_var PHP_VER)
${env_name} global $(get_var PHP_VER)
${env_name} rehash

## default packages:
mkdir ${HOME}/.composer
ln -sf ${script_dir}/../etc/php/composer.json ${HOME}/.composer/composer.json
cd ${HOME}/.composer
composer install

unset env_name env_root script_dir
# vim: set ts=2 sw=2 et:
