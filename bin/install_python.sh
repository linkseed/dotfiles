#!/bin/bash
# pyenv: https://github.com/pyenv/pyenv

env_name="pyenv"
script_dir=$(dirname $(realpath $0))
anyenv_cmd="${HOME}/.anyenv/bin/anyenv"

[ -z $(type -t get_var) ] && source "${script_dir}/../etc/env.sh"

## build dependencies:
# https://github.com/pyenv/pyenv/wiki
if [ $(get_var OS_TYPE) == "GNU/Linux" ] ; then
  $(get_var APT_GET) update && $(get_var APT_GET) install -y \
      make build-essential libssl-dev zlib1g-dev libbz2-dev \
      libreadline-dev libsqlite3-dev wget curl llvm \
      libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev \
      libffi-dev liblzma-dev
elif [ $(get_var OS_TYPE) == "Android" ] ; then
  # TODO: termux required package
  $(get_var APT_GET) update && $(get_var APT_GET) install -y
      libandroid-support ncurses readline libffi openssl libutil \
      libbz2 libsqlite gdbm ncurses-ui-libs libcrypt liblzma
fi

if [ -z $(type -P ${env_name}) ]; then
  ${anyenv_cmd} install ${env_name}
  eval "$(${anyenv_cmd} init -)"

  # WARNING: pyenv init - no longer sets PATH. #1906
  #
  # https://blog.serverworks.co.jp/2021/05/12/233520
  # https://github.com/pyenv/pyenv/issues/1906
  export PYENV_ROOT="$HOME/.anyenv/envs/pyenv"
  export PATH="$PYENV_ROOT/bin:$PATH"
  eval "$(pyenv init --path)"
  if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
  fi
fi

env_root=$(${env_name} root)

## plugins:
# pyenv-default-packages:
# https://github.com/jawshooah/pyenv-default-packages
git clone https://github.com/jawshooah/pyenv-default-packages.git \
    ${env_root}/plugins/pyenv-default-packages
# pyenv-virtualenv:
# https://github.com/pyenv/pyenv-virtualenv
git clone https://github.com/pyenv/pyenv-virtualenv.git \
    ${env_root}/plugins/pyenv-virtualenv

## default packages:
ln -sf ${script_dir}/../etc/python/default-packages \
    ${env_root}/default-packages

##
${env_name} install $(get_var PYTHON_VER)
${env_name} global $(get_var PYTHON_VER)
${env_name} rehash

pip install --upgrade pip

unset anyenv_cmd env_name env_root script_dir
# vim: set ts=2 sw=2 et:
