#!/bin/bash
# nodenv: https://github.com/nodenv/nodenv

env_name="nodenv"
script_dir=$(dirname $(realpath $0))
anyenv_cmd="${HOME}/.anyenv/bin/anyenv"

[ -z $(type -t get_var) ] && source "${script_dir}/../etc/env.sh"

## build dependencies:
# https://github.com/nodejs/node/blob/master/BUILDING.md
$(get_var APT_GET) update && $(get_var APT_GET) install -y \
  git python g++ make

if [ -z $(type -P ${env_name}) ]; then
  ${anyenv_cmd} install ${env_name}
  eval "$(${anyenv_cmd} init -)"
fi

env_root=$(${env_name} root)

cd ${env_root} && src/configure && make -C src

## plugins:

## default packages:
ln -sf ${script_dir}/../etc/nodejs/default-packages \
    ${env_root}/default-packages

##
${env_name} install $(get_var NODE_VER)
${env_name} global $(get_var NODE_VER)
${env_name} rehash

unset anyenv_cmd env_name env_root script_dir
# vim: set ts=2 sw=2 et:
