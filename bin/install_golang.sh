#!/bin/bash
# goenv: https://github.com/syndbg/goenv

env_name="goenv"
script_dir=$(dirname $(realpath $0))
anyenv_cmd="${HOME}/.anyenv/bin/anyenv"

[ -z $(type -t get_var) ] && source "${script_dir}/../etc/env.sh"

## build dependencies:
# $(get_var APT_GET) update && $(get_var APT_GET) install -y bash git

if [ -z $(type -P ${env_name}) ]; then
  ${anyenv_cmd} install ${env_name}
  eval "$(${anyenv_cmd} init -)"
fi

#env_root=$(${env_name} root)

## plugins:

## default packages:
# ln -sf ${script_dir}/../etc/go/default-packages \
#     ${env_name}/default-packages

##
${env_name} install $(get_var GO_VER)
${env_name} global $(get_var GO_VER)
${env_name} rehash

unset anyenv_cmd env_name env_root script_dir
# vim: set ts=2 sw=2 et:
