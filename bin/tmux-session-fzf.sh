#!/bin/bash
#
# Select a tmux session with fzf
# https://qiita.com/hokita222/items/b5d0b168e53d737f4d37

SELECTED="$(tmux list-sessions | fzf | cut -d : -f 1)"

if [ -n "$SELECTED" ]; then
    if [ -n "$TMUX" ]; then
        tmux switch -t $SELECTED
    else
        tmux attach -t $SELECTED
    fi
fi
