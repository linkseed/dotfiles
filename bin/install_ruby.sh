#!/bin/bash
# rbenv: https://github.com/rbenv/rbenv

env_name="rbenv"
script_dir=$(dirname $(realpath $0))
anyenv_cmd="${HOME}/.anyenv/bin/anyenv"

[ -z $(type -t get_var) ] && source "${script_dir}/../etc/env.sh"

## build dependencies:
# https://github.com/rbenv/ruby-build/wiki#suggested-build-environment
if [ $(get_var OS_TYPE) == "GNU/Linux" ] ; then
  $(get_var APT_GET) update && $(get_var APT_GET) install -y \
    autoconf bison build-essential libssl-dev libyaml-dev \
    libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev \
    libgdbm6 libgdbm-dev
elif [ $(get_var OS_TYPE) == "Android" ] ; then
  # TODO: termux required package
  echo "I have not examined it yet."
  #$(get_var APT_GET) update && $(get_var APT_GET) install -y packages
fi

if [ -z $(type -P ${env_name}) ]; then
  ${anyenv_cmd} install ${env_name}
  eval "$(${anyenv_cmd} init -)"
fi

env_root=$(${env_name} root)

## plugins:
# rbenv-default-gems: 
# https://github.com/rbenv/rbenv-default-gems
git clone https://github.com/rbenv/rbenv-default-gems.git \
    ${env_root}/plugins/rbenv-default-gems

## default packages:
ln -sf ${script_dir}/../etc/ruby/default-gems ${env_root}/default-gems

# require gem pg
$(get_var APT_GET) install -y libpq-dev
# require gem sqlite3
$(get_var APT_GET) install -y libsqlite3-dev

##
${env_name} install $(get_var RUBY_VER)
${env_name} global $(get_var RUBY_VER)
${env_name} rehash

unset anyenv_cmd env_name env_root script_dir
# vim: set ts=2 sw=2 et:
