function post_install() {
  source ${PEARL_PKGDIR}/etc/env.sh

  local execfile
  for execfile in $(ls ${PEARL_PKGDIR}/bin)
  do
    link_to_path "${PEARL_PKGDIR}/bin/${execfile}"
    chmod +x "${PEARL_PKGDIR}/bin/${execfile}"
  done

  [ ! -d ${HOME}/.ssh/conf.d ] && \
    mkdir -p ${HOME}/.ssh/conf.d && chmod -R 700 ${HOME}/.ssh
  [ ! -e ${HOME}/.ssh/config ] && touch ${HOME}/.ssh/config
  chmod 600 "${PEARL_PKGDIR}/ssh_config"
  apply "Include ${PEARL_PKGDIR}/ssh_config" "${HOME}/.ssh/config" false

  link vim  "${PEARL_PKGDIR}/config.vim" false
  link_to "${PEARL_PKGDIR}/eslintrc.json" "${HOME}/.eslintrc.json"
  link_to "${PEARL_PKGDIR}/stylelintrc.json" "${HOME}/.stylelintrc.json"

  link_to "${PEARL_PKGDIR}/markdownlint.json" "${HOME}/.markdownlint.json"
  link_to "${PEARL_PKGDIR}/textlintrc.json" "${HOME}/.textlintrc"
  link_to "${PEARL_PKGDIR}/prh.yml" "${HOME}/prh.yml"

  link tmux "${PEARL_PKGDIR}/tmux.conf"
  [ ! -d ${HOME}/.tmuxp ] && mkdir -p ${HOME}/.tmuxp
  link_to "${PEARL_PKGDIR}/tmuxp.yaml" "${HOME}/.tmuxp/default.yaml"

  # git config
  git config --global ghq.root "$(get_var GOPATH)/src"
  git config --global merge.tool vimdiff

  # pearl
  [ -e ${HOME}/.config/pearl/pearl.conf ] && \
    mv -f ${HOME}/.config/pearl/pearl.conf ${HOME}/.config/pearl/pearl.conf.backup
  link_to ${PEARL_PKGDIR}/pearl.conf ${HOME}/.config/pearl/pearl.conf

  return 0
}

function post_update() {
  post_install
}

function pre_remove() {
  local execfile
  for execfile in $(ls ${PEARL_PKGDIR}/bin)
  do
    unlink_from_path "${PEARL_PKGDIR}/bin/${execfile}"
  done

  unlink vim  "${PEARL_PKGDIR}/config.vim"

  unlink tmux "${PEARL_PKGDIR}/tmux.conf"
  unlink_from "${PEARL_PKGDIR}/tmuxp.yaml" "${HOME}/.tmuxp/default.yaml"

  unlink_from "${PEARL_PKGDIR}/eslintrc.json" "${HOME}/.eslintrc.json"
  unlink_from "${PEARL_PKGDIR}/stylelintrc.json" "${HOME}/.stylelintrc.json"

  unlink_from "${PEARL_PKGDIR}/markdownlint.json" "${HOME}/.markdownlint.json"
  unlink_from "${PEARL_PKGDIR}/textlintrc.json" "${HOME}/.textlintrc"
  unlink_from "${PEARL_PKGDIR}/prh.yml" "${HOME}/prh.yml"

  unapply "Include ${PEARL_PKGDIR}/ssh_config" "${HOME}/.ssh/config" false

  git config --global --unset ghq.root
  git config --global --unset merge.tool

  unlink_from ${PEARL_PKGDIR}/pearl.conf ${HOME}/.config/pearl/pearl.conf
  [ -e ${HOME}/.config/pearl/pearl.conf.backup ] && \
    mv -f ${HOME}/.config/pearl/pearl.conf.backup ${HOME}/.config/pearl/pearl.conf

  unset execfile

  return 0
}

# vim: set ts=2 sw=2 et:
