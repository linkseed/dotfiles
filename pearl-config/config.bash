
## Variables
export GOPATH=${HOME}
export PAGER=less
export PATH=${HOME}/bin:${HOME}/.local/bin:${PATH}
# goenv
[ `type -P goenv` ] && export GOENV_DISABLE_GOPATH=1
# cquery
[ -d "${HOME}/src/github.com/cquery-project/cquery/build/release" ] && \
    export PATH=${PATH}:${HOME}/src/github.com/cquery-project/cquery/build/release/bin
# composer
[ -d "${HOME}/.composer/vendor/bin" ] && \
    export PATH=${HOME}/.composer/vendor/bin:${PATH}

export LESS='-R'

# https://github.com/cheat/cheat
export CHEAT_USE_FZF=true

## Alias
[ `type -P ccat` ] && alias cat='ccat --bg=dark'
# [ `type -P htop` ] && alias top='htop'
alias ls='ls --color=always'

[ `type -P ag` ] && alias ag='ag --color'
alias egrep='egrep --color=always'
alias fgrep='fgrep --color=always'
alias grep='grep --color=always'
if [ `type -P vim` ] ; then
  alias vi='vim'
  export EDITOR=vim
fi

[ `type -P systemctl` ] && alias suspend='sudo systemctl suspend'

## Functions & Keybindings

# for termux
if [ `uname -o` == "Android" ] ; then
  # https://wiki.termux.com/wiki/Termux-exec
  export LD_PRELOAD=${PREFIX}/lib/libtermux-exec.so
fi
