set encoding=utf-8
scriptencoding utf-8

set completeopt=menuone,longest,preview,noinsert

" Encode Setting
" set fileencodings=iso-2022-jp,cp932,sjis,euc-jp,utf-8
" set fileformats=unix,dos,mac

" Load settings for each location.
" https://vim-jp.org/vim-users-jp/2009/12/27/Hack-112.html
augroup vimrc-local
  autocmd!
  autocmd BufNewFile,BufReadPost * call s:vimrc_local(expand('<afile>:p:h'))
augroup END

function! s:vimrc_local(loc)
  let files = findfile('.vimrc.local', escape(a:loc, ' ') . ';', -1)
  for i in reverse(filter(files, 'filereadable(v:val)'))
    source `=i`
  endfor
endfunction

" nospell(Side effects of pearl/dot-vim)
au BufNewFile,BufReadPost,FilterReadPost,FileReadPost  * set nospell

" https://github.com/junegunn/fzf.vim
command! -bang -nargs=* GGrep
  \ call fzf#vim#grep(
  \   'git grep --line-number -- '.shellescape(<q-args>), 0,
  \   fzf#vim#with_preview({'dir': systemlist('git rev-parse --show-toplevel')[0]}), <bang>0)

" https://dev.to/serhatteker/vim-fzf-project-root-3jfi
function! s:find_git_root()
  return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
endfunction

command! ProjectFiles execute 'Files' s:find_git_root()

" Keymappings
map <C-p> :ProjectFiles<CR>

map <leader>o :copen<CR>
map <leader>x :cclose<CR>
map <leader>ft :NERDTreeToggle<CR>
noremap <Leader>ad :ALEGoToDefinition<CR>
noremap <Leader>af :ALEFix<CR>
noremap <Leader>ar :ALEFindReferences<CR>
nmap <leader>b :Buffers<CR>
nmap <leader>f :Files<CR>
nmap <leader>t :Tags<CR>

inoremap <C-f> <Right>
inoremap <C-b> <Left>

" Use yaskkserv
"let g:eskk#server = {
"  \ 'host': '192.168.0.1',
"  \ 'port': 1178,
"  \ 'encoding': 'euc-jp',
"  \ 'timeout': 1000,
"  \ 'type': 'dictionary',
"  \}

" ale Settings
let g:ale_completion_enabled = 1
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
let g:ale_sign_column_always = 1
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
" let g:ale_linters = {
"   \ 'c': ['ccls'],
"   \ 'cpp': ['ccls'],
"   \ 'go': ['bingo'],
"   \ 'javascript': ['tsserver'],
"   \ 'php': ['langserver', 'phpcs'],
"   \ 'python': ['pyls'],
"   \ 'ruby': ['solargraph'],
"   \ 'sh': ['language_server'],
"   \ 'typescript': ['tsserver'],
"   \}

" let g:ale_c_ccls_init_options = { 'cache': { 'directory': '.ccls-cache' } }
" let g:ale_cpp_ccls_init_options = { 'cache': { 'directory': '.ccls-cache' } }
