FROM linkseed/debian-base:latest

# ENV USER=worker \
#     HOME=/home/work \
#     LANG=en_US.UTF-8 \
#     UID=10001 \
#     GID=10001

RUN sudo apt-get update -y && sudo apt-get upgrade -y
# Dependencies
RUN sudo sudo apt-get install -y \
        python3 python3-pip bash git \
        coreutils grep sed \
        curl ca-certificates && \
    sudo update-ca-certificates --fresh

# Cleanup
# RUN sudo apt-get autoremove -y && sudo apt-get clean -y && \
#     sudo rm -rf /var/lib/apt/lists/*

# Install Docker
# https://docs.docker.com/install/linux/docker-ce/debian/
RUN sudo apt-get remove docker docker-engine docker.io containerd runc & \
    sudo apt-get update && sudo apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg2 \
        software-properties-common && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - && \
    if [ `uname -m` = "x86_64" ] ; then \
        sudo add-apt-repository \
            "deb [arch=amd64] https://download.docker.com/linux/debian \
            $(lsb_release -cs) \
            stable"; \
    elif [ `uname -m` = "armv7l" ] ; then \
        sudo add-apt-repository \
            "deb [arch=armhf] https://download.docker.com/linux/debian \
            $(lsb_release -cs) \
            stable"; \
    fi && \
    sudo apt-get update && \
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io && \
    sudo usermod -aG docker ${USER}

# Cleanup
RUN sudo apt-get autoremove -y && sudo apt-get clean -y && \
    sudo rm -rf /var/lib/apt/lists/*

# Install Dotfiles
# https://bitbucket.org/linkseed/dotfiles/
# RUN curl -L http://bit.ly/2GWyazJ | bash
COPY ./install.sh ${HOME}/tmp/
RUN sudo chown ${UID}:${GID} ${HOME}/tmp/install.sh && \
    /bin/bash ${HOME}/tmp/install.sh && rm ${HOME}/tmp/install.sh

VOLUME ["${HOME}/tmp"]
WORKDIR ${HOME}/

CMD ["bash", "-l"]
