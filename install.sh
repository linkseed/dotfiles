#!/bin/bash
## Dependencies
# python3-pip, bash, git, GNU coreutilis, grep,sed
if [ ! `type -P pip3` ] ; then
  echo "pip3 is requied."
  exit
fi

## pearl: https://github.com/pearl-core/pearl
PEARL_VERSION="2.3.3"
PEARL_ROOT=${HOME}/.local/share/pearl
PEARL_HOME=${HOME}/.config/pearl

if [ ! `type -P pearl` ] ; then
  pip3 install --user pearl==${PEARL_VERSION}
  export PATH=${HOME}/.local/bin:$PATH
  pearl init
fi

# add a Pearl repository
echo 'PEARL_PACKAGES["my-dotfiles"]={ "url": "https://bitbucket.org/linkseed/dotfiles.git" }' \
    >> ${PEARL_HOME}/pearl.conf
# docker run --rm -it -v ${PWD}:/home/work/tmp linkseed/debian-base
# echo 'PEARL_PACKAGES["my-dotfiles"]={ "url": "/home/work/tmp" }' \
#     >> ${PEARL_HOME}/pearl.conf

## my-dotfiles: https://bitbucket.org/linkseed/dotfiles
pearl install my-dotfiles
source ${PEARL_ROOT}/packages/local/my-dotfiles/etc/env.sh

# Base packages
$(get_var APT_GET) update
$(get_var APT_GET) upgrade -y
$(get_var APT_GET) install -y $(get_var PACKAGES)
if [ $(get_var OS_TYPE) == "Android" ]; then
  # Do not use *env in termux
  $(get_var APT_GET) install -y golang python python-dev ruby
  exec $SHELL -l
fi

pearl install linkseed/anyenv
export PATH="${HOME}/.anyenv/bin:${PATH}"
eval "$(anyenv init -)"

# tmuxp:
pip3 install --user tmuxp

# install golang
# Note: curl: (60) SSL certificate problem: `sudo update-ca-certificates --fresh`
source ${PEARL_ROOT}/packages/local/my-dotfiles/bin/install_golang.sh

## go tools
export GOPATH=$(get_var GOPATH)
export GOENV_DISABLE_GOPATH=1
# ghq:
# go get -u github.com/motemen/ghq
go get -u github.com/x-motemen/ghq

# ccat:
go get -u github.com/jingweno/ccat

# cheat:
go get -u github.com/cheat/cheat/cmd/cheat

## bash
# https://github.com/pearl-hub/dot-bash
pearl install dot-bash
# https://github.com/pearl-hub/cylon
pearl install cylon

## vim
# https://github.com/pearl-hub/dot-vim
# NOTE: Excluded for autobuild
# pearl install dot-vim

pearl install nerdtree
pearl install linkseed/ale
pearl install linkseed/commentary.vim
pearl install linkseed/fzf.vim
pearl install linkseed/surround.vim
pearl install linkseed/unimpaired.vim

## tmux
# https://github.com/pearl-hub/dot-tmux
# NOTE: Excluded for autobuild
# pearl install dot-tmux
# https://github.com/pearl-hub/tpm
pearl install tpm

# misc
# https://github.com/pearl-hub/dot-git
pearl install dot-git
pearl install gitgutter
# https://github.com/pearl-hub/ranger
pearl install ranger
# powerline: https://github.com/pearl-hub/powerline
$(get_var APT_GET) install -y fontconfig
[ $(get_var OS_TYPE) == "GNU/Linux" ] &&  $(get_var APT_GET) install -y fonts-powerline
# NOTE: Excluded for autobuild
# pearl install powerline


## Cleaning up
$(get_var APT_GET) autoremove -y
$(get_var APT_GET) clean -y

exit 0
# vim: set ts=2 sw=2 et:
