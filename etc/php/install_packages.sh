#!/bin/bash

script_dir=$(dirname $(realpath $0))

[ ! -d ${HOME}/.composer ]  && mkdir ${HOME}/.composer
cd ${HOME}/.composer

ln -sf ${script_dir}/composer.json ${HOME}/.composer/composer.json

composer install

export PATH=${HOME}/.composer/vendor/bin:${PATH}

## Lint
# PHP_CodeSniffer
# composer global require "squizlabs/php_codesniffer=*"

# WPCS (WordPress Coding Standards for PHP_CodeSniffer)
# https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards
phpcs --config-set installed_paths ${HOME}/.composer/vendor/wp-coding-standards/wpcs
ln -sf ${script_dir}/phpcs.xml ${HOME}/.phpcs.xml

# Add the following to VS Code Workspace Settings
# "phpcs.standard": "${HOME}/.config/pearl/packages/local/my-dotfiles/etc/php/wp-phpcs.xml"


# Language server
# It is now included in PHP IntelliSense
# composer require felixfbecker/language-server


## Test
# composer require --dev phpunit/phpunit ^6.5
# composer require --dev phpunit/php-invoker
# composer require --dev phpunit/dbunit


## Debug
#sudo pecl install xdebug

# vim: set ts=2 sw=2 noet:
