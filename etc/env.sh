#!/bin/bash

declare -A VARS
## Variables
VARS["GOPATH"]=${HOME}
VARS["ANYENV_ROOT"]=${HOME}/.anyenv
# Version
VARS["GO_VER"]="1.16.3"
VARS["NODE_VER"]="14.17.0"
VARS["PHP_VER"]="7.3.13"
VARS["PYTHON_VER"]="3.7.6"
VARS["RUBY_VER"]="2.6.5"
# Misc
VARS["OS_TYPE"]=`uname -o`
if [ ${VARS["OS_TYPE"]} == "GNU/Linux" ]; then
  ## Debian

  VARS["APT_GET"]='sudo apt'
  VARS["PACKAGES"]="
    sudo locales gnupg2 unzip
    vim-nox tmux bash-completion
    ctags git tig mercurial silversearcher-ag
    man openssh-client mosh
    dnsutils htop procps
    curl w3m ca-certificates
    build-essential
    fontconfig
    "
  VARS["LDFLAGS"]=""
elif [ ${VARS["OS_TYPE"]} == "Android" ]; then
  ## Termux
  # move ~/src to external sdcard
  # ln -sf /storage/sdcard1/Android/data/com.termux/files ${HOME}/src

  VARS["APT_GET"]='apt'
  VARS["PACKAGES"]="
    gnupg unzip
    vim-python tmux bash-completion
    ctags git tig silversearcher-ag
    man openssh mosh
    dnsutils htop procps
    curl w3m ca-certificates
    clang libclang llvm make
    fontconfig-utils
    "
  VARS["LDFLAGS"]="-lm -lcompiler_rt"
fi

function get_var() {
  [ -z "$1" ] && return null
  local value=${VARS[$1]}
  [ -z "$value" ] && return 33
  echo $value
}
# vim: set ts=2 sw=2 et:
